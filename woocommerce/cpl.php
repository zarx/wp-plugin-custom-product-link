<?php
/**
 * Custom Linked Products
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $_wc_cpl_current_list;

if (empty($_wc_cpl_current_list)) return;

$products = wc_cpl_get_products($_wc_cpl_current_list['id']);

if ( count($products) > 0 ) : ?>

    <div class="cpl-list cpl-<?= $_wc_cpl_current_list['id'] ?> products">

        <h2><?= $_wc_cpl_current_list['label'] ?></h2>

        <?php woocommerce_product_loop_start(); ?>

            <?php foreach ( $products as $product ) : $post = get_post($product->id); setup_postdata($post); ?>

                <?php wc_get_template_part( 'content', 'product' ); ?>

            <?php endforeach; // end of the loop. ?>

        <?php woocommerce_product_loop_end(); ?>

    </div>

<?php 

endif;

wp_reset_query();