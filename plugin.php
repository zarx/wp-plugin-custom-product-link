<?php
/*
Plugin Name: WooCommerce Custom Product Links
Plugin URI: http://johan.ninja/
Description: Adds custom product links
Version: 1.0
Author: Johan Boström
Author URI: http://johan.ninja/
*/

DEFINE('WC_CPL_PLUGIN_URL',         plugins_url('',__FILE__));
DEFINE('WC_CPL_PLUGIN_DIR',         plugin_dir_path(__FILE__));
DEFINE('WC_CPL_PLUGIN_BASENAME',    plugin_basename(__FILE__));
DEFINE('WC_CPL_PREFIX',             '_wc_cpl_');

include WC_CPL_PLUGIN_DIR . "/inc/wc.cpl.php";

global $_wc_cpl_list;
$_wc_cpl_list = get_option('_wc_cpl_list', array());
if (!is_array($_wc_cpl_list)) $_wc_cpl_list = array();

add_action( 'woocommerce_process_product_meta', '_wc_cpl_save_products', 10, 2 );
add_action( 'woocommerce_product_options_related', '_wc_cpl_select_products');
add_action( 'woocommerce_after_single_product_summary', '_cpl_woocommerce_after_single_product_summary', 25, 0);

function _wc_cpl_add_menus() {
    add_submenu_page('edit.php?post_type=product', __('Product links', 'cpl'), __('Product links', 'cpl'), 'administrator', '_wc_cpl_settings_page', '_wc_cpl_settings_page');
}

function _wc_cpl_settings_page() {  
    require WC_CPL_PLUGIN_DIR . '/inc/settings.php';
}
add_action("admin_menu", "_wc_cpl_add_menus");  