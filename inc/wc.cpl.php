<?php
function _wc_cpl_select_products() {
	global $post, $_wc_cpl_list;
    ?>
	<div class="options_group">
    <?php
    foreach ($_wc_cpl_list as $link) {
        $id = WC_CPL_PREFIX . $link['id'];
        //$product_ids = wc_cpl_get_product_ids($link['id'], $post->ID);
        $products = wc_cpl_get_products($link['id'], $post->ID);
        $product_ids = array();
        $data_selected = new stdClass();
        foreach ($products as $product) {
            $prod_id = $product->id;
            $data_selected->$prod_id = $product->get_formatted_name();
            $product_ids[] = $prod_id;
        }
        json_encode($data_selected);
	?>
		<p class="form-field">
            <label for="<?= $id ?>"><?php echo $link['label'] ?></label>
            <input type="hidden" 
                   class="wc-product-search" 
                   style="width: 50%;" 
                   id="<?= $id ?>" 
                   name="<?= $id ?>" 
                   data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce' ); ?>" 
                   data-action="woocommerce_json_search_products_and_variations" 
                   data-multiple="true" 
                   data-selected="<?= htmlentities(json_encode($data_selected)); ?>" 
                   value="<?= implode(',', $product_ids) ?>">
            
            <img class="help_tip" data-tip='<?php echo $link['help_text'] ?>' src="<?php echo WC()->plugin_url(); ?>/assets/images/help.png" height="16" width="16" />
        </p>
    <?php
    }
    ?>
    </div>
<?php
}

function _wc_cpl_save_products( $post_id, $post ) {
    global $_wc_cpl_list;
    foreach ($_wc_cpl_list as $link) {
        $meta_id = WC_CPL_PREFIX . $link['id'];
        if ( isset( $_POST[$meta_id] ) ) {
            $product_ids = array();
            $posted_ids = explode(',', $_POST[$meta_id]);
            foreach ( $posted_ids as $product_id ) {
                if ( $product_id && $product_id > 0 ) { $product_ids[] = $product_id; }
            }
            update_post_meta( $post_id, $meta_id, $product_ids );
        } else {
            delete_post_meta( $post_id, $meta_id );
        }
    }
}

function wc_cpl_get_product_ids($list_id, $post_id = null) {
    global $_wc_cpl_list, $post;    
    global $sitepress;
    
    $post_obj = empty($post_id) ? $post : get_post($post_id);
    
    if (function_exists('icl_object_id')) {
        $icl_obj_id = icl_object_id($post_obj->ID, 'product', true, $sitepress->get_default_language());
        $post_obj = get_post($icl_obj_id);
    }
    
    
    $id = WC_CPL_PREFIX . $list_id;
    $data_ids = get_post_meta( $post_obj->ID, $id, true );
    
    $product_ids = ! empty( $data_ids ) ? array_map( 'absint',  $data_ids ) : null;
    
    if (function_exists('icl_object_id') && is_array($product_ids)) {
        $product_ids_lang = array();
        foreach ($product_ids as $product_id) {
            $lang_prod_id = icl_object_id($product_id, 'product', false, ICL_LANGUAGE_CODE);
            if ($lang_prod_id) {
                $product_ids_lang[] = $lang_prod_id;
            }
        }
        $product_ids = $product_ids_lang;
    }
    
    if (empty($product_ids) || !is_array($product_ids))
    { return array(); }
    
    return $product_ids;
}

function wc_cpl_get_products($list_id, $post_id = null) {
    $product_ids = wc_cpl_get_product_ids($list_id, $post_id);
    $products = array();
    foreach ($product_ids as $product_id) {
        $products[] = get_product( $product_id );
    }
    return $products;
}

function wc_cpl_get_template_part($slug, $name = '') {
    $template = '';

    // Look in yourtheme/slug-name.php and yourtheme/woocommerce/slug-name.php
    if ( $name && ! WC_TEMPLATE_DEBUG_MODE ) {
        $template = locate_template( array( "{$slug}-{$name}.php", WC()->template_path() . "{$slug}-{$name}.php" ) );
    }

    // Get default slug-name.php
    if ( ! $template && $name && file_exists( WC()->plugin_path() . "/templates/{$slug}-{$name}.php" ) ) {
        $template = WC()->plugin_path() . "/templates/{$slug}-{$name}.php";
    }

    // If template file doesn't exist, look in yourtheme/slug.php and yourtheme/woocommerce/slug.php
    if ( ! $template && ! WC_TEMPLATE_DEBUG_MODE ) {
        $template = locate_template( array( "{$slug}.php", WC()->template_path() . "{$slug}.php" ) );
    }

    // Allow 3rd party plugin filter template file from their plugin
    if ( ( ! $template && WC_TEMPLATE_DEBUG_MODE ) || $template ) {
        $template = apply_filters( 'wc_get_template_part', $template, $slug, $name );
    }

    if ( ! $template && $name && file_exists( WC_CPL_PLUGIN_DIR . WC()->template_path() . "{$slug}-{$name}.php" ) ) {
        $template = WC_CPL_PLUGIN_DIR . WC()->template_path() . "{$slug}-{$name}.php";
    }
    
    if ( ! $template && $name && file_exists( WC_CPL_PLUGIN_DIR . WC()->template_path() . "{$slug}.php" ) ) {
        $template = WC_CPL_PLUGIN_DIR . WC()->template_path() . "{$slug}.php";
    }
    
    if ( $template ) {
        load_template( $template, false );
    }
}

function _cpl_woocommerce_after_single_product_summary() {
    global $_wc_cpl_current_list, $_wc_cpl_list;
    if ( is_array($_wc_cpl_list) ) {
        foreach ($_wc_cpl_list as $_wc_cpl_current_list) {
            wc_cpl_get_template_part('cpl', 'id');
        }
    }
}