<?php
    global $_wc_cpl_list;
    
    if ($_GET['action'] == 'add_row') {
        array_unshift($_wc_cpl_list, array());
        //$_wc_cpl_list[] = array();
    } elseif ($_POST['action'] == 'save') {
        $_wc_cpl_list = $_POST['_wc_cpl_list'];
        update_option('_wc_cpl_list', $_wc_cpl_list);
        echo '<div class="updated">Settings saved</div>';
    }
?>


<div class="wrap" id="poststuff">
    <form method="POST" action="?post_type=<?= $_GET['post_type'] ?>&page=<?= $_GET['page'] ?>">
        <input name="action" value="save" type="hidden" />
        <h2>
            Custom product links
            <a href="?post_type=<?= $_GET['post_type'] ?>&page=<?= $_GET['page'] ?>&action=add_row" class="add-new-h2">Add row</a>
        </h2>
        <div class="inside">
        <?php if (count($_wc_cpl_list) > 0): $i = 0; foreach($_wc_cpl_list as $list): ?>
            <table class="form-table">
                <tbody>
                    <tr>
                        <th>Namn<p class="description">Visas i adminläge</p></th>
            <td><input type="text" name="_wc_cpl_list[<?= $i ?>][name]" value="<?= $list['name'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Id<p class="description">Används vid uthämtning</p></th>
                        <td><input type="text" name="_wc_cpl_list[<?= $i ?>][id]" value="<?= $list['id'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Publik rubrik<p class="description">Visas vid utlistning</p></th>
                        <td><input type="text" name="_wc_cpl_list[<?= $i ?>][label]" value="<?= $list['label'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Hjälptext<p class="description">Visas som hjälptext vid listan</p></th>
                        <td><textarea name="_wc_cpl_list[<?= $i ?>][help_text]"><?= $list['help_text'] ?></textarea></td>
                    </tr>
                </tbody>
            </table>
            <hr />
        <?php $i++; endforeach; ?>
            <input type="submit" class="button button-primary" value="Save all" />
        <?php else: ?>
            Inga produkt-länkningar
        <?php endif; ?>
        </div>
    </form>
</div>